<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-12
 * Time: 15:56
 */

namespace Dmytro\Brand\Model\Brand;


use Dmytro\Brand\Model\ResourceModel\Brand\CollectionFactory;

/**
 * Class DataProvider
 * @package Dmytro\Brand\Model\Brand
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    protected $collection;
    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $brandCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $brandCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $brandCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }


    public function getData()
    {
        if (!isset($this->loadedData)){
            $items = $this->collection->getItems();
            $this->loadedData = array();
            foreach ($items as $item){
                $this->loadedData[$item->getId()] = $item->getData();
            }
        }
        return $this->loadedData;
    }
}