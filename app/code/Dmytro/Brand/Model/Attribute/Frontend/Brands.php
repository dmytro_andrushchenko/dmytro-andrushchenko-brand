<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-04
 * Time: 23:23
 */

namespace Dmytro\Brand\Model\Attribute\Frontend;


use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;

/**
 * Class Brands
 * @package Dmytro\Brand\Model\Attribute\Frontend
 */
class Brands extends AbstractFrontend
{

    /**
     * @param \Magento\Framework\DataObject $object
     * @return mixed|string
     */
    public function getValue(\Magento\Framework\DataObject $object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        return "<b>$value</b>";
    }

}