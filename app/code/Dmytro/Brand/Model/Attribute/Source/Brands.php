<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-04
 * Time: 23:13
 */

namespace Dmytro\Brand\Model\Attribute\Source;


use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Dmytro\Brand\Model\ResourceModel\Brand\CollectionFactory;

/**
 * Class Brands
 * @package Dmytro\Brand\Model\Attribute\Source
 */
class Brands extends AbstractSource
{

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;


    /**
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Add options for attribute
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->collectionFactory->create();
        if (!$this->_options) {
            foreach ($collection as $value){
                $this->_options[] = ['label' => $value->getName(), 'value' => $value->getBrand_id()];
            }
        }
        return $this->_options;
    }

}