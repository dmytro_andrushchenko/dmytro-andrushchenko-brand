<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-07
 * Time: 12:22
 */

namespace Dmytro\Brand\Model;
use Dmytro\Brand\Api\Data\BrandInterface;
use Dmytro\Brand\Model\ResourceModel\Brand as ResourceBrand;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Brand
 * @package Dmytro\Brand\Model
 */
class Brand extends AbstractModel implements BrandInterface
{

    protected function _construct()
    {
        $this->_init(ResourceBrand::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('brand_id');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->getData('logo');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData('description');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData('brand_id', $id);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @param string $logo
     * @return $this
     */
    public function setLogo($logo)
    {
        return $this->setData('logo', $logo);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }
}

