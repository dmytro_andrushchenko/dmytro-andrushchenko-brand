<?php
namespace Dmytro\Brand\Model;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\StateException;
use Dmytro\Brand\Api\BrandRepositoryInterface;
use Dmytro\Brand\Api\Data\BrandInterface;
use Dmytro\Brand\Model\ResourceModel\Brand as BrandResource;
use Dmytro\Brand\Api\Data;
use Dmytro\Brand\Api\Data\BrandSearchResultInterface;
use Dmytro\Brand\Api\Data\BrandSearchResultInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Dmytro\Brand\Model\ResourceModel\Brand\CollectionFactory as BrandCollectionFactory;

class BrandRepository implements BrandRepositoryInterface
{
    /**
     * @var array
     */
    private $registry = [];
    /**
     * @var \Dmytro\Brand\Model\BrandFactory
     */
    private $brandFactory;
    /**
     * @var BrandResource
     */
    private $brandResource;
    /**
     * @var BrandCollectionFactory
     */
    private $brandCollectionFactory;
    /**
     * @var BrandSearchResultInterfaceFactory
     */
    private $brandSearchResultFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * BrandRepository constructor.
     * @param BrandResource $brandResource
     * @param \Dmytro\Brand\Model\BrandFactory $brandFactory
     * @param BrandCollectionFactory $brandCollectionFactory
     * @param BrandSearchResultInterfaceFactory $brandSearchResultFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        BrandResource $brandResource,
        BrandFactory $brandFactory,
        BrandCollectionFactory $brandCollectionFactory,
        BrandSearchResultInterfaceFactory $brandSearchResultFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        $this->brandResource = $brandResource;
        $this->brandFactory = $brandFactory;
        $this->brandCollectionFactory = $brandCollectionFactory;
        $this->brandSearchResultFactory = $brandSearchResultFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }
    /**
     * @param Data\BrandInterface $brand
     * @return BrandInterface
     * @throws StateException
     */
    public function save(Data\BrandInterface $brand)
    {
        try {
            if (empty($brand->getId())) {
                $brand->setId(null);
            }
            /** @var Brand $brand */
            $this->brandResource->save($brand);
            //$this->registry[$brand->getId()] = $this->getById($brand->getId());
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save brand #%1', $brand->getId()));
        }
       // return $this->registry[$brand->getId()];
        return $brand;
    }

    /**
     * @param $brandId
     * @return \Magento\Framework\DataObject|mixed|null
     */
    public function getById($brandId)
    {
        $parameter = $this->brandCollectionFactory->create();
        return $parameter->getItemById($brandId);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return BrandSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->brandCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        /** @var BrandSearchResultInterface $searchResult */
        $searchResult = $this->brandSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param BrandInterface $brand
     * @return bool
     * @throws StateException
     */
    public function delete(Data\BrandInterface $brand)
    {
        try {
            /** @var Brand $brand */
            $this->brandResource->delete($brand);
            unset($this->registry[$brand->getId()]);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove brand #%1', $brand->getId()));
        }
        return true;
    }

    /**
     * @param $brandId
     * @return mixed
     * @throws StateException
     */
    public function deleteById($brandId)
    {
        return $this->delete($this->getById($brandId));
    }

    /**
     * @param string $name
     * @return BrandSearchResultInterface
     */
    public function getByName($name)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('name', $name)->create();
        return $this->getList($searchCriteria);
    }

}