<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-07
 * Time: 12:10
 */

namespace Dmytro\Brand\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Brand extends AbstractDb
{

    protected function _construct()
    {
        $this->_init('dmytro_brand','brand_id');
    }
}