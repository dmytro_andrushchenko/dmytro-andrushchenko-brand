<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-07
 * Time: 12:20
 */

namespace Dmytro\Brand\Model\ResourceModel\Brand;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = 'brand_id';

    protected function _construct()
    {
        $this->_init(
          'Dmytro\Brand\Model\Brand',
          'Dmytro\Brand\Model\ResourceModel\Brand'
        );
    }
}