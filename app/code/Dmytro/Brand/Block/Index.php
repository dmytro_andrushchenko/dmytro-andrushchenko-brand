<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-09
 * Time: 14:12
 */

namespace Dmytro\Brand\Block;

use Dmytro\Brand\Helper\Data;
use Dmytro\Brand\Model\BrandRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Index
 * @package Dmytro\Brand\Block
 */
class Index extends Template
{

    /**
     * @var BrandRepository
     */
    protected $brandRepository;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Index constructor.
     * @param Context $context
     * @param BrandRepository $brandRepository
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        BrandRepository $brandRepository,
        Data $helper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    )
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->helper = $helper;
        $this->brandRepository = $brandRepository;
        parent::__construct($context, $data);
    }

    /**
     * Get list of brands
     * @return \Dmytro\Brand\Api\Data\BrandSearchResultInterface
     */
    public function getList()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        return $this->brandRepository->getList($searchCriteria);
    }


    /**
     * @return mixed
     */
    public function congratulationText()
    {
        return $this->helper->getGeneralConfig('congratulation');

    }
}