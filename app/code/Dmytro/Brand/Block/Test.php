<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-09-30
 * Time: 18:17
 */

namespace Dmytro\Brand\Block;



use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Cms\Model\PageFactory;

/**
 * Class Test
 * @package Dmytro\Brand\Block
 */
class Test extends Template
{

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var PageFactory
     */
    private $pageFactory;


    /**
     * Test constructor.
     * @param Context $context
     * @param ProductRepository $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param PageFactory $pageFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        PageFactory $pageFactory,
        array $data = [])
    {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->pageFactory = $pageFactory;
        parent::__construct($context, $data);

    }

    /**
     * Get products which price is more than 100
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    public function getProducts()
    {
        $this->searchCriteriaBuilder->addFilter('price', '100','gteq');
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $products = $this->productRepository
            ->getList($searchCriteria)
            ->getItems();

        return $products;
    }



}