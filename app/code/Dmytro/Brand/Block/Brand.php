<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-07
 * Time: 14:39
 */

namespace Dmytro\Brand\Block;

use Dmytro\Brand\Model\BrandRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\RequestInterface;

class Brand extends Template
{
    /**
     * @var BrandRepository
     */
    protected $brandRepository;

    /**
     * @var RequestInterface
     */
    protected $request;


    /**
     * Brand constructor.
     * @param Context $context
     * @param BrandRepository $brandRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        BrandRepository $brandRepository,
        RequestInterface $request,
        array $data = [])
    {
        $this->request = $request;
        $this->brandRepository = $brandRepository;
        parent::__construct($context, $data);
    }


    /**
     * Get brand by id
     * @return \Magento\Framework\DataObject
     */
    public function getBrand()
    {
        $param=$this->getRequest();
        $brand = $this->brandRepository->getById($param);
        return $brand;
    }

    /**
     * Get parameter from id/[parameter]
     * @return RequestInterface|mixed
     */
    public function getRequest()
    {
        $param = $this->request->getParam('id', null);
        return $param;
    }

    public function getName()
    {
        return $this->getBrand()->getName();
    }
}