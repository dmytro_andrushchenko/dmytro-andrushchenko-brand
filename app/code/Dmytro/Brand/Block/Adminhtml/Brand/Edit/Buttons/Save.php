<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-13
 * Time: 14:46
 */

namespace Dmytro\Brand\Block\Adminhtml\Brand\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Save
 * @package Dmytro\Brand\Block\Adminhtml\Brand\Edit\Buttons
 */
class Save extends Generic implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save Brand'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
