<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-13
 * Time: 14:43
 */


namespace Dmytro\Brand\Block\Adminhtml\Brand\Edit\Buttons;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;


/**
 * Class Reset
 * @package Dmytro\Brand\Block\Adminhtml\Profile\Edit\Buttons
 */
class Reset implements ButtonProviderInterface
{
    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload();',
            'sort_order' => 30
        ];
    }
}
