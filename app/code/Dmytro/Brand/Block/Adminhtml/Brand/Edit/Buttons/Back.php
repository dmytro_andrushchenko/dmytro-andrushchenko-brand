<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-13
 * Time: 14:34
 */

namespace Dmytro\Brand\Block\Adminhtml\Brand\Edit\Buttons;


use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Back
 * @package Dmytro\Brand\Block\Adminhtml\Brand\Edit\Buttons
 */
class Back extends Generic implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getUrl('*/*/')),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}