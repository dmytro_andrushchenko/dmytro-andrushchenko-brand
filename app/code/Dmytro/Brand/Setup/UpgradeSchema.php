<?php

namespace Dmytro\Brand\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package Dmytro\Brand\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrade table dmytro_brand
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
//        $setup->startSetup();
//        if (version_compare($context->getVersion(), '2.0.0', '<'))
//        {
//            $setup->getConnection()->addColumn(
//                $setup->getTable('dmytro_brand'),
//                'description',
//                [
//                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                    'length' => 65535,
//                    'nullable' => true,
//                    'default' => '',
//                    'comment' => 'Description of brand'
//                ]
//            );
//        }
//        $setup->endSetup();
    }
}

