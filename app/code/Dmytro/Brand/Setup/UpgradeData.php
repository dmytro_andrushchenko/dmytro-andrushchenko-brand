<?php

namespace Dmytro\Brand\Setup;



use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;


/**
 * Class UpgradeData
 * @package Dmytro\Brand\Setup
 */
class UpgradeData implements UpgradeDataInterface
{


    /**
     * Upgrade data for dmytro_brand table
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

//        $setup->startSetup();
//        if (version_compare($context->getVersion(), '1.0.1', '<'))
//        {
//            $table = $setup->getTable('dmytro_brand');
//            $setup->getConnection()
//                ->insertForce($table, ['name' => 'Reebok', 'description' => 'Reebok International Limited is an American footwear and clothing company founded in Bolton, and headquartered in Boston']);
//
//        }
//
//
//        if (version_compare($context->getVersion(), '1.0.2', '<'))
//        {
//            $table = $setup->getTable('dmytro_brand');
//
//            $data = [
//                [
//                    'name' => 'Under Armour',
//                    'description' => 'Under Armour, Inc. is an American company that manufactures footwear, sports, and casual apparel.'
//                ],
//                [
//                    'name' => 'New Balance',
//                    'description' => 'New Balance is an American sports footwear and apparel brand that has been around since 1906.'
//                ],
//            ];
//
//
//            foreach ($data as $bind){
//                $setup->getConnection()
//                    ->insertForce($table, $bind);
//            }
//
//            $setup->getConnection()
//                ->update($table, ['description' => 'Puma SE, branded as Puma, is a German multinational corporation that designs and manufactures athletic and casual footwear, apparel and accessories, which is headquartered in Herzogenaurach, Bavaria, Germany.'],
//                    'brand_id in (3)');
//
//            $setup->getConnection()
//                ->update($table, ['description' => 'NIKE is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services.'],
//                    'brand_id in (2)');
//
//            $setup->getConnection()
//                ->update($table, ['description' => 'Adidas is a German multinational corporation, founded and headquartered in Herzogenaurach, Germany, that designs and manufactures shoes, clothing and accessories.'],
//                    'brand_id in (1)');
//
//
//        }
//
//
//        $setup->endSetup();
  }
}