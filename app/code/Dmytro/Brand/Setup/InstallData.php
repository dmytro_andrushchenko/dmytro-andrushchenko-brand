<?php
namespace Dmytro\Brand\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;


use Dmytro\Brand\Model\Attribute\Frontend\Brands as Frontend;
use Dmytro\Brand\Model\Attribute\Source\Brands as Source;

/**
* @codeCoverageIgnore
*/
class InstallData implements InstallDataInterface
{

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;


    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Install data for dmytro_brand table
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $eavSetup =$this->eavSetupFactory->create();

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'brands',
            [
                'group' => 'General',
                'type' => 'varchar',
                'label' => 'Brands',
                'input' => 'select',
                'source' => Source::class,
                'frontend' => Frontend::class,
                'required' => false,
                'sort_order' => 50,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible' => true,
                'is_html_allowed_on_front' => true,
                'visible_on_front' => true
            ]
        );

//        $data = [
//                    ['name' => 'Adidas'],
//                    ['name' => 'Nike'],
//                    ['name' => 'Puma'],
//                ];
//        foreach ($data as $bind) {
//        $setup->getConnection()
//        ->insertForce($setup->getTable('dmytro_brand'), $bind);
//        }
    }
}
