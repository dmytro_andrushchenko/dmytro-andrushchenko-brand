<?php

namespace Dmytro\Brand\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Dmytro\Brand\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install data for dmytro_brand table
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
//        /**
//        * Create table 'dmytro_brand'
//        */
//        $table = $setup->getConnection()
//        ->newTable($setup->getTable('dmytro_brand'))
//        ->addColumn(
//        'brand_id',
//        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//        null,
//        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
//        'Brand ID'
//        )
//        ->addColumn(
//        'name',
//        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//        255,
//        ['nullable' => false, 'default' => ''],
//        'Name of brand'
//        )
//        ->addColumn(
//        'logo',
//        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//        255,
//        ['nullable' => true, 'default' => ''],
//        'Brand logo'
//        )
//        ->addColumn(
//        'created_at',
//        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
//        null,
//        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
//        'Created At'
//        )
//            ->setComment("Brands table");
//        $setup->getConnection()->createTable($table);
    }
}
