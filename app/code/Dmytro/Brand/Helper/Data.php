<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-16
 * Time: 16:14
 */

namespace Dmytro\Brand\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Dmytro\Brand\Helper
 */
class Data extends AbstractHelper
{

    const CONFIG_BRAND_PATH = 'brand/';

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * @param $code
     * @param null $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null)
    {

        return $this->getConfigValue(self::CONFIG_BRAND_PATH .'general/'. $code, $storeId);
    }
}