<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-19
 * Time: 14:22
 */

namespace Dmytro\Brand\Observer;

use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Dmytro\Brand\Helper\Email;

/**
 * Class MyObserver
 * @package Dmytro\Brand\Observer
 */
class MyObserver implements ObserverInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Email
     */
    private $email;

    /**
     * MyObserver constructor.
     * @param Session $session
     * @param Email $email
     */
    public function __construct(
        Session $session,
        Email $email
    )
    {
        $this->session = $session;
        $this->email = $email;
    }

    /**
     * Send message to email when total product's quantity more than 5
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $totalQty = $this->session->getQuote()->getItemsQty();
        if ($totalQty > 5){
            return $this->email->sendEmail();
        }

    }
}