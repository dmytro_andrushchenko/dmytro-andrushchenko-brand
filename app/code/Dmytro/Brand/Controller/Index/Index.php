<?php

namespace Dmytro\Brand\Controller\Index;

use Dmytro\Brand\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;

/**
 * Class Index
 * @package Dmytro\Brand\Controller\Index
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var PageFactory
     */
    protected $helper;

    /**
     * Index constructor.
     * @param Data $helper
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Data $helper,
        Context $context,
        PageFactory $resultPageFactory
        )
    {
        $this->helper = $helper;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Init Page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $brandConfig = $this->helper->getGeneralConfig('enable');
        if ($brandConfig) {
            return $this->resultPageFactory->create();
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('/');

    }

}
