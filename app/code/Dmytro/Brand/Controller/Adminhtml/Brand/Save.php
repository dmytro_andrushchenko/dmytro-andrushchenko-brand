<?php

namespace Dmytro\Brand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;
use Dmytro\Brand\Model\BrandFactory;

/**
 * Class Save
 * @package Dmytro\Brand\Controller\Adminhtml\Brand
 */
class Save extends Action
{

    /**
     * @var BrandFactory
     */
    private $brandModel;


    /**
     * Save constructor.
     * @param Action\Context $context
     * @param BrandFactory $brandModel
     */
    public function __construct(
        Action\Context $context,
        BrandFactory $brandModel
    )
    {
        $this->brandModel = $brandModel;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|
     * \Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $brand = $this->brandModel->create();
        $brand->setData($this->getRequest()->getParams());
        $brand->save();
        return $this->resultRedirectFactory->create()->setPath('*/*');
    }
}