<?php

namespace Dmytro\Brand\Plugin;

/**
 * Class Copyright
 * @package Dmytro\Brand\Plugin
 */
class Copyright
{
    /**
     * Change Copyright
     * @return string
     */
    public function aroundGetCopyright()
    {
        return "Copyright © 2020 Andrushchenko";
    }

    /**
     * Add additional information to the copyright
     * @param \Magento\Theme\Block\Html\Footer $subject
     * @param $result
     * @return string
     */
    public function afterGetCopyright(
        \Magento\Theme\Block\Html\Footer $subject,
        $result
    )
    {
        return '||||||' . $result . '||||||';
    }
}