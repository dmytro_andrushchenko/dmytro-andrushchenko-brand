<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-27
 * Time: 12:35
 */

namespace Dmytro\Brand\Plugin;

/**
 * Class CartLog
 * @package Dmytro\Brand\Plugin
 */
class CartLog
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    private $quote;

    /**
     * CartLog constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->quote = $checkoutSession->getQuote();
        $this->logger = $logger;
    }

    /**
     * Log each add to cart action
     * @param $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return array
     */
    public function afterAddProduct(
        $subject,
        $productInfo,
        $requestInfo = null)
    {
        $sku = $requestInfo->getSku();
        $qty = $requestInfo->getQty();

        $this->logger->info(__('Added new product to the cart '. $sku .'. Quantity - ' .$qty));

        return array($productInfo, $requestInfo);
    }

    /**
     * Log each remove from cart action
     * @param $subject
     * @param $itemId
     * @return null
     */
    public function beforeRemoveItem(
        $subject,
        $itemId)
    {
        $item = $this->quote->getItemById($itemId);
        $sku = $item->getSku();
        $qty = $item->getQty();
        $this->logger->info(__('Removed product from the cart '. $sku .'. Quantity - ' . $qty));
        return NULL;
    }
}