<?php


namespace Dmytro\Brand\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface BrandSearchResultInterface
 * @package Dmytro\Brand\Api\Data
 */
interface BrandSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Dmytro\Brand\Api\Data\BrandInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return \Dmytro\Brand\Api\Data\BrandSearchResultInterface
     */
    public function setItems(array $items);

}