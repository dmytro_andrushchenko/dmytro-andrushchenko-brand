<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-19
 * Time: 10:16
 */

namespace Dmytro\Brand\Api\Data;


interface BrandInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getLogo();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param string $logo
     * @return $this
     */
    public function setLogo($logo);

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

}