<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2020-10-19
 * Time: 10:13
 */

namespace Dmytro\Brand\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BrandRepositoryInterface
{
    /**
     * Save brand
     *
     * @param Data\BrandInterface $brand
     * @return \Dmytro\Brand\Api\Data\BrandInterface
     */
    public function save(Data\BrandInterface $brand);

    /**
     * Get brand by id
     *
     * @param $brandId
     * @return mixed
     */
    public function getById($brandId);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Dmytro\Brand\Api\Data\BrandSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);


    /**
     * Delete brand
     *
     * @param Data\BrandInterface $brand
     * @return mixed
     */
    public function delete(Data\BrandInterface $brand);

    /**
     * Delete brand by id
     *
     * @param $brandId
     * @return mixed
     */
    public function deleteById($brandId);

    /**
     * @param string $name
     * @return \Dmytro\Brand\Api\Data\BrandSearchResultInterface
     */
    public function getByName($name);


}