<?php


namespace Dmytro\BrandGraphQl\Model\Resolver;


use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class BrandCreate
 * @package Dmytro\BrandGraphQl\Model\Resolver
 */
class BrandCreate implements ResolverInterface
{
    /**
     * @var DataProvider\Brand
     */
    private $brandDataProvider;

    /**
     * BrandCreate constructor.
     * @param DataProvider\Brand $brandDataProvider
     */
    public function __construct(
        \Dmytro\BrandGraphQl\Model\Resolver\DataProvider\Brand $brandDataProvider
    ) {
        $this->brandDataProvider = $brandDataProvider;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return \Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws \Magento\Framework\Exception\StateException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $name = $args['input']['name'];
        $logo = $args['input']['logo'];
        $description = $args['input']['description'];

        $success_message = $this->brandDataProvider->brandCreate(
            $name,
            $logo,
            $description
        );
        return $success_message;
    }
}