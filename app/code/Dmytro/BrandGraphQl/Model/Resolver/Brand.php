<?php


namespace Dmytro\BrandGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Brand
 * @package Dmytro\BrandGraphQl\Model\Resolver
 */
class Brand implements ResolverInterface
{
    /**
     * @var DataProvider\Brand
     */
    private $brandDataProvider;

    /**
     * Brand constructor.
     * @param DataProvider\Brand $brandDataProvider
     */
    public function __construct(
        \Dmytro\BrandGraphQl\Model\Resolver\DataProvider\Brand $brandDataProvider
    ) {
        $this->brandDataProvider = $brandDataProvider;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        return $this->brandDataProvider->getBrand();
    }
}