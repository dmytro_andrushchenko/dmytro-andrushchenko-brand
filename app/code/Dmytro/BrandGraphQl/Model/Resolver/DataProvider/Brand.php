<?php


namespace Dmytro\BrandGraphQl\Model\Resolver\DataProvider;

use Dmytro\Brand\Api\Data\BrandInterface;
use Magento\Framework\Exception\StateException;

class Brand
{
    /**
     * @var \Dmytro\Brand\Model\BrandFactory
     */
    protected $brandFactory;

    /**
     * @var \Dmytro\Brand\Model\BrandRepository
     */
    protected $brandRepository;

    /**
     * Brand constructor.
     * @param \Dmytro\Brand\Model\BrandFactory $brandFactory
     * @param \Dmytro\Brand\Model\BrandRepository $brandRepository
     */
    public function __construct(
        \Dmytro\Brand\Model\BrandFactory $brandFactory,
        \Dmytro\Brand\Model\BrandRepository $brandRepository
    )
    {
        $this->brandFactory  = $brandFactory;
        $this->brandRepository  = $brandRepository;
    }


    /**
     * Get brand
     * @return array
     */
    public function getBrand( )
    {
        try {
            $collection = $this->brandFactory->create()->getCollection();
            $brandData = $collection->getData();

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $brandData;
    }

    /**
     * Create new brand
     * @param $name
     * @param $logo
     * @param $description
     * @return mixed
     * @throws StateException
     */
    public function brandCreate($name, $logo, $description)
    {
        try {
            $model = $this->brandFactory->create();
            $model->setData('name', $name);
            $model->setData('logo', $logo);
            $model->setData('description', $description);
            $this->brandRepository->save($model);
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        $thanks_message['success_message']="Thanks For Creating New Brand";
        return $thanks_message;

    }

}