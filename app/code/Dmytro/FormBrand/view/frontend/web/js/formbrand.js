

        define(
            [
                'uiComponent',
                'ko',
                'jquery',
                'Magento_Ui/js/modal/modal'
            ],
            function(
                Component,
                ko,
                $,
                modal
            ) {

                return Component.extend({
                    initialize: function () {
                        this._super();
                        // component initialization logic
                        return this;
                    },

                    // click at button and open form
                    openForm: ko.observable(false),
                    formOpen:function (){
                        this.openForm(true);
                    },


                    //submit form and send information to ajax controller
                    nameBrand: ko.observable(""),
                    logo: ko.observable(""),
                    description: ko.observable(""),
                    hasBeenSubmitted: ko.observable(false),

                    formSubmit: function (){
                        console.log('submitted');
                        console.log(this.hasBeenSubmitted());
                        var payload = {
                            nameBrand: this.nameBrand(),
                            logo: this.logo(),
                            description: this.description(),
                        }
                        this.hasBeenSubmitted(true);
                        $.ajax({
                            method: "POST",
                            url: "formbrand/ajax/formbrand",
                            data: { name: payload['nameBrand'], logo: payload['logo'], description: payload['description']},
                            dataType: "json"
                        }).done(function( msg ) {
                            console.log(msg);
                        });
                        console.log(this.hasBeenSubmitted);

                    },


                });

            }
        );

