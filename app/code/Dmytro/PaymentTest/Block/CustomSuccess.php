<?php


namespace Dmytro\PaymentTest\Block;

use Magento\Framework\View\Element\Template;
use Dmytro\PaymentTest\Helper\OrderInfo;

class CustomSuccess extends Template
{

    /**
     * @var OrderInfo
     */
    private $orderInfo;


    /**
     * CustomSuccess constructor.
     * @param OrderInfo $orderInfo
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        OrderInfo $orderInfo,
        Template\Context $context,
        array $data = [])
    {

        $this->orderInfo=$orderInfo;
        parent::__construct($context, $data);
    }

    /**
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->orderInfo->getCurrentPayment()->getLastTransId();
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->orderInfo->getCurrentPayment()->getMethod();
    }


}