<?php


namespace Dmytro\PaymentTest\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Dmytro\PaymentTest\Api\Liqpay\ResponseInterface;

/**
 * Class LiqpayCallback
 * @package Dmytro\PaymentTest\Controller\Checkout
 */
class LiqpayCallback extends Action
{
    /**
     * @var ResponseInterface
     */
    private $responseLiqpay;

    /**
     * LiqpayCallback constructor.
     * @param ResponseInterface $responseLiqpay
     * @param Context $context
     */
    public function __construct(
        ResponseInterface $responseLiqpay,
        Context $context
    )
    {
        $this->responseLiqpay = $responseLiqpay;
        parent::__construct($context);
    }


    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $this->responseLiqpay->setData($data);
        return $this->resultFactory->create(ResultFactory::TYPE_JSON);
    }
}