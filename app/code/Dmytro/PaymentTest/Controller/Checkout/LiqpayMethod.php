<?php


namespace Dmytro\PaymentTest\Controller\Checkout;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Dmytro\PaymentTest\Helper\Liqpay\Data as LiqpayHelper;
use Dmytro\PaymentTest\Api\Liqpay\RequestInterface;

/**
 * Class LiqpayMethod
 * @package Dmytro\PaymentTest\Controller\Checkout
 */
class LiqpayMethod extends Action
{
    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var LiqpayHelper
     */
    private $liqpayHelper;

    /**
     * @var RequestInterface
     */
    private $requestLiqpay;

    /**
     * LiqpayMethod constructor.
     * @param RequestInterface $requestLiqpay
     * @param LiqpayHelper $liqpayHelper
     * @param ResultFactory $resultFactory
     * @param Context $context
     */
    public function __construct(
        RequestInterface $requestLiqpay,
        LiqpayHelper $liqpayHelper,
        ResultFactory $resultFactory,
        Context $context
    )
    {
        $this->requestLiqpay = $requestLiqpay;
        $this->liqpayHelper = $liqpayHelper;
        $this->resultFactory = $resultFactory;
        parent::__construct($context);
    }



    public function execute()
    {
        try {
            if (!$this->liqpayHelper->isEnabled()) {
                throw new \Exception('Payment is not allow.');
            }

            $payment_info = $this->requestLiqpay->sendRequest();
            $data = [
                'status' => 'success',
                'payment_info' => $payment_info,
            ];

        } catch (\Exception $e){
            $this->messageManager->addExceptionMessage($e, 'Some problems with Liqpay method');
            $data = [
                'status' => 'error'
            ];
        }


        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($data);
        return $result;
    }
}