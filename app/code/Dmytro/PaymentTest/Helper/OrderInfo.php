<?php


namespace Dmytro\PaymentTest\Helper;


use Magento\Checkout\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class OrderInfo
 * @package Dmytro\PaymentTest\Helper
 */
class OrderInfo extends AbstractHelper
{
    /**
     * @var SessionFactory
     */
    private $checkoutSessionFactory;

    /**
     * OrderInfo constructor.
     * @param SessionFactory $checkoutSessionFactory
     * @param Context $context
     */
    public function __construct(
        SessionFactory $checkoutSessionFactory,
        Context $context
    )
    {
        $this->checkoutSessionFactory=$checkoutSessionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getCurrentOrder()
    {
        return $this->checkoutSessionFactory->create()->getLastRealOrder();
    }


    /**
     * @return float|\Magento\Sales\Api\Data\OrderPaymentInterface|mixed|null
     */
    public function getCurrentPayment()
    {
        return $this->getCurrentOrder()->getPayment();
    }


}