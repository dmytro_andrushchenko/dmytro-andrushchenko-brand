<?php


namespace Dmytro\PaymentTest\Helper\Liqpay;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Dmytro\PaymentTest\Helper\Liqpay
 */
class Data extends AbstractHelper
{

    const CONFIG_ACTIVE_LIQPAY_PATH = 'payment/liqpay_method/active';
    const CONFIG_PUBLIC_KEY_LIQPAY_PATH = 'payment/liqpay_method/public_key';
    const CONFIG_PRIVATE_KEY_LIQPAY_PATH = 'payment/liqpay_method/private_key';

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * Check if module is enabled by admin
     * @return bool
     */
    public function isEnabled()
    {
        if ($this->scopeConfig->getValue(self::CONFIG_ACTIVE_LIQPAY_PATH, ScopeInterface::SCOPE_STORE)){
            if ($this->getPublicKeyLiqpay() && $this->getPrivateKeyLiqpay()){
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Get liqpay public key
     * @return mixed
     */
    public function getPublicKeyLiqpay()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PUBLIC_KEY_LIQPAY_PATH,ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get liqpay private key
     * @return mixed
     */
    public function getPrivateKeyLiqpay()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PRIVATE_KEY_LIQPAY_PATH,ScopeInterface::SCOPE_STORE);
    }
}