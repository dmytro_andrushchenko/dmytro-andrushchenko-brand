<?php


namespace Dmytro\PaymentTest\Helper;

use Magento\Checkout\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class QuoteInfo
 * @package Dmytro\PaymentTest\Helper
 */
class QuoteInfo extends AbstractHelper
{
    /**
     * @var SessionFactory
     */
    private $checkoutSessionFactory;

    /**
     * QuoteInfo constructor.
     * @param SessionFactory $checkoutSessionFactory
     * @param Context $context
     */
    public function __construct(
        SessionFactory $checkoutSessionFactory,
        Context $context
    )
    {
        $this->checkoutSessionFactory=$checkoutSessionFactory;
        parent::__construct($context);
    }

    /**
     * Get Current Quote from Session
     * @return \Magento\Quote\Model\Quote
     */
    public function getCurrentQuote()
    {
        return $this->checkoutSessionFactory->create()->getQuote();
    }

    /**
     * @return mixed|null
     */
    public function getAmount()
    {
        return $this->getCurrentQuote()->getData('grand_total');
    }

    /**
     * @return mixed|null
     */
    public function getCurrencyCode()
    {
        return $this->getCurrentQuote()->getData('quote_currency_code');
    }

    /**
     * @return mixed|null
     */
    public function getEntityId()
    {
        return $this->getCurrentQuote()->getData('entity_id');
    }



}