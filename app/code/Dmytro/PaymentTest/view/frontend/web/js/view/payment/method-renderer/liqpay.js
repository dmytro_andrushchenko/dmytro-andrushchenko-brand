define([
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
    ],
    function (
        $,
        Component,
        url,
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Dmytro_PaymentTest/payment/liqpay',
            },

            context: function() {
                return this;
            },

            getCode: function() {
                return 'liqpay_method';
            },

            isActive: function() {
                return true;
            },


            getLiqpay: function (){
                let self = this;

                $.post(url.build('payment/checkout/liqpaymethod'),{

                }).done(function (liqpay){
                    console.log(liqpay);
                    if (liqpay.status == 'success'){
                        var x = document.getElementById("liqpay_checkout");
                        if (x.style.display === "block") {
                            x.style.display = "none";
                        } else {
                            x.style.display = "block";
                            LiqPayCheckout.init({
                                data: liqpay.payment_info.data,
                                signature: liqpay.payment_info.signature,
                                embedTo: "#liqpay_checkout",
                                mode: "embed"
                            }).on("liqpay.callback", function(data){
                                if (data.status == 'success'){
                                    if (self.placeOrder()){
                                        setTimeout(function(){ self.sendInfo(data); }, 3000);
                                    }

                                }
                            }).on("liqpay.ready", function(data){
                                console.log('ready');
                            }).on("liqpay.close", function(data){
                                console.log('close');
                            });
                        }
                    }
                })
            },


            sendInfo: function (data){
                $.ajax({
                    method: "POST",
                    url: url.build("payment/checkout/liqpaycallback"),
                    data: data,
                    dataType: "json",
                })
            },


            test: function (){
                console.log('11112222');

            },

        });
    }
);