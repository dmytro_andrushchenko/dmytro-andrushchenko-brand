define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'liqpay_method',
                component: 'Dmytro_PaymentTest/js/view/payment/method-renderer/liqpay'
            }
        );

        return Component.extend({
        });
    });