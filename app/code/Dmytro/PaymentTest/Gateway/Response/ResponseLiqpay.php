<?php


namespace Dmytro\PaymentTest\Gateway\Response;


use Dmytro\PaymentTest\Api\Liqpay\ResponseInterface;
use Dmytro\PaymentTest\Sdk\Liqpay;
use Dmytro\PaymentTest\Helper\OrderInfo;
use Dmytro\PaymentTest\Api\Liqpay\CallbackInterface;

class ResponseLiqpay implements ResponseInterface
{
    private $liqpay;
    private $orderInfo;
    private $liqpayCallback;

    public function __construct(
        Liqpay $liqpay,
        OrderInfo $orderInfo,
        CallbackInterface $liqpayCallback

    )
    {
        $this->liqpayCallback = $liqpayCallback;
        $this->orderInfo = $orderInfo;
        $this->liqpay = $liqpay;
    }

    public function setData($responseData)
    {
        if ($this->checkSignature($responseData)){
            $order = $this->orderInfo->getCurrentOrder();
            $this->liqpayCallback->callback($order, $responseData);
            return true;
        };
        return false;
    }

    protected function checkSignature($checkData)
    {
        return $this->liqpay->checkSignature($checkData['data'], $checkData['signature']);
    }
}