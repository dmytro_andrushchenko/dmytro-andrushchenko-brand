<?php


namespace Dmytro\PaymentTest\Gateway\Request;

use Dmytro\PaymentTest\Helper\QuoteInfo;

use Dmytro\PaymentTest\Api\Liqpay\RequestInterface;
use Dmytro\PaymentTest\Sdk\Liqpay as LiqpaySdk;

/**
 * Class RequestLiqpay
 * @package Dmytro\PaymentTest\Gateway\Request
 */
class RequestLiqpay implements RequestInterface
{
    /**
     * @var QuoteInfo
     */
    private $currentQuote;

    /**
     * @var LiqpaySdk
     */
    private $liqpaySdk;

    /**
     * RequestLiqpay constructor.
     * @param LiqpaySdk $liqpaySdk
     * @param QuoteInfo $currentQuote
     */
    public function __construct(
        LiqpaySdk $liqpaySdk,
        QuoteInfo $currentQuote
    )
    {
        $this->currentQuote = $currentQuote;
        $this->liqpaySdk = $liqpaySdk;
    }

    /**
     * Return encrypted data and signature
     *
     * @return array
     */
    public function sendRequest()
    {
        $data = $this->getData();
        return $this->liqpaySdk->cnb_form($data);
    }


    /**
     * Return array which has data for creating payment information
     * @return array
     */
    public function getData()
    {
        return array(
            'amount' => $this->currentQuote->getAmount(),
            'currency' => $this->currentQuote->getCurrencyCode(),
            'description' => 'Payment for ' . $this->currentQuote->getEntityId() . ' cart. ',
            'order_id' => $this->currentQuote->getEntityId(),
        );
    }

}