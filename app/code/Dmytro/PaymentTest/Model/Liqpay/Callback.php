<?php


namespace Dmytro\PaymentTest\Model\Liqpay;


use Dmytro\PaymentTest\Api\Liqpay\CallbackInterface;
use Magento\Checkout\Model\Session;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;

/**
 * Class Callback
 * @package Dmytro\PaymentTest\Model\Liqpay
 */
class Callback implements CallbackInterface
{
    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var BuilderInterface
     */
    private $transactionBuilder;

    /**
     * @var OrderPaymentInterface
     */
    private $payment;

    /**
     * @var string|int
     */
    private $transactionId;


    /**
     * Callback constructor.
     * @param Session $checkoutSession
     * @param BuilderInterface $transactionBuilder
     */
    public function __construct(
        Session $checkoutSession,
        BuilderInterface $transactionBuilder
    )
    {
        $this->transactionBuilder = $transactionBuilder;
        $this->checkoutSession=$checkoutSession;

    }

    /**
     * @param OrderInterface $order
     * @param $data
     * @return null
     */
    public function callback($order, $data)
    {
        $this->order = $order;
        $this->payment = $this->order->getPayment();
        $this->transactionId = $data['transaction_id'];
        $this->setPaymentInformation();
        $this->setTransaction();
    }

    /**
     * Set Payment Information (transaction Id)
     */
    public function setPaymentInformation()
    {
        $this->payment->setLastTransId($this->transactionId);
        $this->payment->setTransactionId($this->transactionId);
        $this->payment->setParentTransactionId(null);
        $this->payment->save();
    }

    /**
     * Create and set Transaction Information
     */
    public function setTransaction()
    {
        $trans = $this->transactionBuilder;
        $trans->setPayment($this->payment)
            ->setOrder($this->order)
            ->setTransactionId($this->transactionId)
            ->setFailSafe(true)
            ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);

        $this->order->save();
    }
}