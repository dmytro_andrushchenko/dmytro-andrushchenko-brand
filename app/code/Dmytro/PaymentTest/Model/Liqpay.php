<?php


namespace Dmytro\PaymentTest\Model;

use \Magento\Payment\Model\Method\AbstractMethod;

/**
 * Class Liqpay
 * @package Dmytro\PaymentTest\Model
 */
class Liqpay extends AbstractMethod
{

    const PAYMENT_LIQPAY_METHOD_CODE = 'liqpay_method';
    protected $_code = self::PAYMENT_LIQPAY_METHOD_CODE;


}
