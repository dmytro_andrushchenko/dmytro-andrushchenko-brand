<?php
namespace Dmytro\PaymentTest\Sdk;


class Liqpay
{
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_RUB = 'RUB';
    const CURRENT_VERSION = 3;
    const CURRENT_ACTION = 'pay';


    /**
     * @var string[]
     */
    protected $supportedCurrencies = array(
        self::CURRENCY_EUR,
        self::CURRENCY_USD,
        self::CURRENCY_UAH,
        self::CURRENCY_RUB,
    );
    /**
     * Public key
     * @var mixed
     */
    private $public_key;

    /**
     * Private key
     * @var mixed
     */
    private $private_key;

    /**
     * @var \Dmytro\PaymentTest\Helper\Liqpay\Data
     */
    private $helperLiqpay;


    /**
     * Liqpay constructor.
     * @param \Dmytro\PaymentTest\Helper\Liqpay\Data $helperLiqpay
     */
    public function __construct(
        \Dmytro\PaymentTest\Helper\Liqpay\Data $helperLiqpay
    )
    {
        $this->helperLiqpay = $helperLiqpay;
        if ($helperLiqpay->isEnabled()) {
            $this->public_key = $helperLiqpay->getPublicKeyLiqpay();
            $this->private_key = $helperLiqpay->getPrivateKeyLiqpay();
        }
    }

    /**
     * Compare request and response signatures
     * @param $data
     * @param $signature
     * @return bool
     */
    public function checkSignature($data, $signature)
    {
        $generatedSignature = base64_encode(sha1($this->private_key . $data . $this->private_key, 1));

        if ($generatedSignature == $signature){
            return true;
        } else{
            return false;
        }
    }

    /**
     * @param $params
     * @return array
     */
    public function cnb_form($params)
    {
        $params = $this->cnb_params($params);
        $data = $this->encode_params($params);
        $signature = $this->cnb_signature($params);

        return array(
            'data' => $data,
            'signature' => $signature,
        );
    }

    /**
     * @param $params
     * @return array
     */
    private function cnb_params($params)
    {
        $params = array(
                'public_key' => $this->public_key,
                'version' => self::CURRENT_VERSION,
                'action' => self::CURRENT_ACTION,
            ) + $params;
        try {
            if (!isset($params['version'])) {
                throw new \Exception('Version is null');
            }
            if (!isset($params['amount'])) {
                throw new \Exception('Amount is null');
            }
            if (!isset($params['currency'])) {
                throw new \Exception('Currency is null');
            }
            if (!in_array($params['currency'], $this->supportedCurrencies)) {
                throw new \Exception('Currency is not supported');
            }
            if (!isset($params['description'])) {
                throw new \Exception('Description is null');
            }
        } catch (\Exception $e) {

        }


        return $params;
    }

    /**
     * @param $params
     * @return string
     */
    private function encode_params($params)
    {
        return base64_encode(json_encode($params));
    }

    /**
     * @param $params
     * @return string
     */
    private function cnb_signature($params)
    {
        $params = $this->cnb_params($params);
        $private_key = $this->private_key;

        $json = $this->encode_params($params);
        $signature = $this->str_to_sign($private_key . $json . $private_key);

        return $signature;
    }

    /**
     * @param $str
     * @return string
     */
    private function str_to_sign($str)
    {
        return base64_encode(sha1($str, 1));
    }
}