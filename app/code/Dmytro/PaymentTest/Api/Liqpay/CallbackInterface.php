<?php

namespace Dmytro\PaymentTest\Api\Liqpay;

use Magento\Sales\Api\Data\OrderInterface;

interface CallbackInterface
{
    /**
     * @param OrderInterface $order
     * @param $data
     * @return null
     */
    public function callback(OrderInterface $order, $data);


    public function setPaymentInformation();

    public function setTransaction();
}