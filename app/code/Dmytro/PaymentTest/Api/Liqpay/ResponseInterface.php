<?php


namespace Dmytro\PaymentTest\Api\Liqpay;


interface ResponseInterface
{

    /**
     * @param $data
     * @return boolean
     */
    public function setData($data);


}