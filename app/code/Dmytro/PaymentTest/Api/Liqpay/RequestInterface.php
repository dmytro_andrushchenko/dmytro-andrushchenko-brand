<?php


namespace Dmytro\PaymentTest\Api\Liqpay;


interface RequestInterface
{
    /**
     * @return array
     */
    public function sendRequest();

    /**
     * @return array
     */
    public function getData();

}